# Important
This project has been put on hiatus. I also moved it to my [dotfiles repository](https://gitlab.com/wolfiy/dotfiles). I am currently using [this](https://gitlab.com/wolfiy/dotfiles/-/tree/master/minimal-arch) setup, which is much more complete.

# shiro themed arch rice
these are the dotfiles, scripts, configs and more i use in my shiro themed arch.
rice is wip and so is the repo. u can check it out anyway if you're interested tho most is incomplete atm.

## screenshots
keep in mind these are wip. 
![kitty](https://i.imgur.com/M4qJxeI.png)kitty&desktop

![startpage](https://i.imgur.com/7IHgRd3.png)startpage

![google](https://i.imgur.com/lc2KnQb.png)google

![duckduckgo](https://i.imgur.com/74NvrhC.png)duckduckgo

![anilist](https://i.imgur.com/SkXCBbv.png)anilist 
